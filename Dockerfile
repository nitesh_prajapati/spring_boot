#BUILD PROJECT STAGE
FROM maven:3.5.3-jdk-10 as builder
ADD pom.xml pom.xml
#ADD config/maven/settings.xml /usr/share/maven/conf/settings.xml
ADD src src
RUN mvn clean install

#SETUP ENV STAGE
FROM openjdk:11-jre-slim

COPY --from=builder target/spring-boot-service.war app.jar

VOLUME /tmp

ENV JAVA_OPTS=""
ENV SERVER_PORT=8089

EXPOSE 8089

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar