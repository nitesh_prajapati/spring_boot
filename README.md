# spring_boot

**Spring boot test application**

---

## Build

The project use [Maven](https://maven.apache.org/) to build, you can compile it using the following command:

```
mvn clean install
```
 
It will generate a WAR file in the target folder **target/member-service.war**.

## Tests

To execute all the tests in the project, you can use the next command:

```
mvn test
```

--- 

## Configuration
 
The service can be configured through the following environment variables:
 
*   SERVER_PORT (8089): define the port to expose the endpoints.

---

## Run

After the build, the project generate a runnable WAR that can be executed as a regular jar:

```
java -jar target/spring-boot-service.war
```

In development you can also use the Spring Boot Maven plugin as follow:

```
mvn spring-boot:run
```

Or 
directly in the IDE execute the class **com.nitesh.spring.MySpringBootApplication**

## Docker

### Docker Build

Alternatively the project support [Docker](https://www.docker.com/), you can execute the above command to compile the repo and build the 
Docker image:

```
docker build -t spring-boot-service .
```

### Docker Config

Ports:
* **8091** to expose Restful endpoints.
Volumes:

* **/var/log/sf/** volume to get the logs from the service.

### Docker Run

After building the docker image you can also run the service using docker with the 
following command:

```
docker run -p 8089:8089 -v /var/log/sf/:/var/log/sf/ --name spring-boot-service spring-boot-service
```

## Features Documentation

Nothing yet.

## Endpoints Documentation
This project use [Swagger 2](https://swagger.io/) to document all the operations, you can use the live UI using the following link:

* http://localhost:8091/swagger-ui.html

## Known Issues

*   No known issues registered yet.

## Bitbuket repo
https://bitbucket.org/nitesh_prajapati/spring_boot/
* git push nitesh_prajapati@bitbucket.org:nitesh_prajapati/spring_boot.git master
* git push http://nitesh_prajapati@bitbucket.org/nitesh_prajapati/spring_boot.git master

## Useful Links

*	Dev Documentation: https://bitbucket.org/smartfocus1/dev-documentation/src/master/
*	Spring Boot docs: https://docs.spring.io/spring-boot/docs/2.0.0.M6/reference/htmlsingle/
*	Spring Data docs: https://docs.spring.io/spring-data/jpa/docs/2.0.0.RELEASE/reference/html/
*	Spring Swagger docs: https://springfox.github.io/springfox/docs/snapshot/