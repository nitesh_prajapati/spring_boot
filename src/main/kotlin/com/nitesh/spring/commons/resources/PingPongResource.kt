package com.nitesh.spring.commons.resources

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
@Api(tags = ["Health Check"], description = "Health Check Endpoints")
class PingPongResource {

    @GetMapping(path = ["/PongPing"])
    @ApiOperation(value = "Complete health check", notes = "")
    fun pongPing(): ResponseEntity<String> {
        return ResponseEntity.ok("PongPing OK")
    }

    @GetMapping(path = ["/PingPong"])
    @ApiOperation(value = "Lightweight health check", notes = "")
    fun pingPong(): ResponseEntity<String> {
        println("hhelloeooeo")
        return ResponseEntity.ok("PingPong OK")
    }

}