package com.nitesh.spring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import java.util.*
import javax.annotation.PostConstruct

@SpringBootApplication
class MySpringBootApplication : SpringBootServletInitializer() {

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(MySpringBootApplication::class.java)
    }

    @PostConstruct
    internal fun started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(MySpringBootApplication::class.java, *args)
}